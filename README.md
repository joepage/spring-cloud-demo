Spring Cloud Eureka Demo
========================

## Overview

The following project is designed to demonstrate microservice registration and discovery using Eureka. It is in the form of a service 
that contains train station names which is called via a separate web service. The web services are registered to a Eureka service.
The project has been implemented using Thymeleaf, Spring, Spring Boot and Spring Cloud with a H2 database and Jetty container. 
The application is made up of the following services:

* A Spring Cloud Eureka registration service which is responsible for service registration and communication.
* A web service which handles ReSTful calls to the stations service.
* The stations microservice which handles ReST calls and returns station data.

## Installation and running

Import the application code as a Maven project into your IDE and run a maven install. In Eclipse you can right click on the project followed by Run As -> Maven Install. The installation process will also run the unit test suite.

As the application runs using Spring Boot the three services can be run by the following methods 
* Select each of the launch files in /java-stations-test/src/main/resources/configurations/, right click -> Run as <service name>
* Alternatively you can run each of the main methods for each service individually: `WebServer` for the main web server, `StationServer` for the main station service and `RegistrationServer` for the Eureka registration service. In each case right click on the class and select Run As -> Java Application.


I suggest you run the Registration service first as you can then check when the other services have registered with it. Once the registration service has started you can go to the Eureka dashboard here [http://localhost:1111](http://localhost:1111). If you scroll down to "Instances currently registered with Eureka" you will see services that are currently registered. When you start the web server and station server you will eventually see these services registered as "WEB-SERVICE" and "STATION-SERVICE" (refresh the dashboard to see updates). While testing the application I have noticed that the time taken to register services can vary so please allow time for the services to appear. Furthermore, once the services have registered there is a period of time where the services exchange	 meta-data which means that even though the services are running and registered the station service might still give a 503 Service Unavailable error. Please be patient as the services will work but may take up to 30 seconds from the point at which they appear in the Eureka dashboard.

Once all services are up and running you can access the main index page here [http://localhost:3333/](http://localhost:3333/). The index page contains links describing some of the functionality available in order to demonstrate calls made via the Eureka registration service.