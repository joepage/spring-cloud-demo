package com.jpage.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;


@Entity
@Table(name = "STATION")
public class Station implements Serializable {

	private static final long serialVersionUID = -1270568195961603647L;

	@Id
	protected Long id;

	@Column
	protected String name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;

		if (!(o instanceof Station)) return false;

		Station station = (Station) o;

		return new org.apache.commons.lang3.builder.EqualsBuilder()
				.append(id, station.id)
				.append(name, station.name)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new org.apache.commons.lang3.builder.HashCodeBuilder(17, 37)
				.append(id)
				.append(name)
				.toHashCode();
	}
}
