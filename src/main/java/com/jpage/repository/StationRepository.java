package com.jpage.repository;

import com.jpage.domain.Station;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Repository for Station data implemented using Spring Data JPA.
 *
 */
public interface StationRepository extends JpaRepository<Station, Long> {

    List<Station> findByNameLike(String name);

}
