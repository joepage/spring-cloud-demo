package com.jpage.dto;

import com.jpage.domain.Station;

import java.io.Serializable;

public class StationDto implements Serializable {

	private static final long serialVersionUID = 8124317562337915581L;
	
	private String name;
	
	public StationDto(Station station){
		this.name = station.getName();
	}
	
	public StationDto(){
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	

}
