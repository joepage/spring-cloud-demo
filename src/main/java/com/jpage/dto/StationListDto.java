package com.jpage.dto;

import com.jpage.domain.Station;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class StationListDto implements Serializable {

	private static final long serialVersionUID = 2138868618340890802L;
	
	private List<StationDto> stations;

    public StationListDto(List<Station> stationsList){

        stations = new ArrayList<StationDto>();

        for(Station station : stationsList){
            stations.add(new StationDto(station));
        }
    }

    public StationListDto(){

    }

    public List<StationDto> getStations() {
        return stations;
    }

    public void setStations(List<StationDto> stations) {
        this.stations = stations;
    }
}
