package com.jpage.services.web;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.logging.Logger;

import javax.naming.ServiceUnavailableException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.jpage.dto.HealthCheckDto;
import com.jpage.dto.StationListDto;
import com.jpage.exception.ServiceNotFoundException;

/**
 * Client controller, fetches Station info from the microservice via
 * {@link WebStationService}.
 *
 */
@Controller
public class WebStationController {

	@Autowired
	protected WebStationService stationService;

	protected Logger logger = Logger.getLogger(WebStationController.class
			.getName());

	public WebStationController(WebStationService stationService) {
		this.stationService = stationService;
	}
	
	@RequestMapping(value = "/station/search", method = RequestMethod.GET, 
			produces = {MediaType.APPLICATION_JSON_VALUE})
	public String searchForm(Model model) {
		model.addAttribute("searchCriteria", new SearchCriteria());
		return "stationSearch";
	}


	@RequestMapping(value = "/station/searchAll", method = RequestMethod.GET,
			produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<StationListDto> searchAll() throws ServiceUnavailableException, IOException {

		StationListDto stations;

		try {
			stations = stationService.findAll();
		} catch(Exception e) {
			throw new ServiceNotFoundException("STATION SERVICE");
		}

		if (stations.getStations().size() > 0){
			return new ResponseEntity<StationListDto>(stations, HttpStatus.OK);
		} else {
			stations = null;
			return new ResponseEntity<StationListDto>(stations, HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping("/station")
	public String goHome() {
		return "index";
	}

	@RequestMapping("/station/nodata")
	public String noData() {
		return "nodata";
	}

	
	@RequestMapping(value = "/station/dosearch", method = RequestMethod.GET,
			produces = {MediaType.APPLICATION_JSON_VALUE})
	public Object doSearch(SearchCriteria criteria) throws URISyntaxException {
		logger.info("web-service search() invoked: " + criteria);

		String stationName = criteria.getStationName();
		if (StringUtils.hasText(stationName)) {

			StationListDto stations;
			try {
				stations = stationService.findByName(stationName);
			} catch(Exception e) {
				throw new ServiceNotFoundException("STATION SERVICE");
			}

			if (stations.getStations().size() > 0){
				return new ResponseEntity<StationListDto>(stations, HttpStatus.OK);
			} else {
				return "redirect: /nodata";
			}
		} else {
			// If nothing is passed to the search in the first place then just return OK
			return new ResponseEntity<HttpStatus>(HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/station/health", method = RequestMethod.GET,
			produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<HealthCheckDto> checkHealth() throws URISyntaxException {
		logger.info("web-service health check invoked");
		HealthCheckDto healthCheck;
			try {
				healthCheck = stationService.healthCheck();
				logger.info("Service is " + healthCheck);
				healthCheck.setDescription(healthCheck.getDescription() + ": STATION SERVICE");
				
			} catch(Exception e) {
				throw new ServiceNotFoundException("STATION SERVICE");
			}
			return new ResponseEntity<HealthCheckDto>(healthCheck, HttpStatus.OK);
	}
}
