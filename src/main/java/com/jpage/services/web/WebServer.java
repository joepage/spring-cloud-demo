package com.jpage.services.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

/**
 * Station web-server. Works as a microservice client, fetching data from the
 * Station-Service. Uses the Discovery Server (Eureka) to find the microservice.
 *
 */
@SpringBootApplication
@EnableDiscoveryClient
// Disable component scanner ...
@ComponentScan(useDefaultFilters = false)
public class WebServer {

	public static final String STATION_SERVICE_URL = "http://station-service";

	/**
	 * Run the application using Spring Boot and an embedded servlet engine.
	 * 
	 */
	public static void main(String[] args) {
		System.setProperty("spring.config.name", "web-server");
		SpringApplication.run(WebServer.class, args);
	}

	/**
	 * The StationService encapsulates the interaction with the micro-service.
	 * 
	 * @return A new service instance.
	 */
	@Bean
	public WebStationService stationService() {
		return new WebStationService(STATION_SERVICE_URL);
	}

	/**
	 * Create the controller, passing it the {@link WebStationService} to use.
	 * 
	 * @return
	 */
	@Bean
	public WebStationController stationController() {
		return new WebStationController(stationService());
	}

	@Bean
	public HomeController homeController() {
		return new HomeController();
	}
}
