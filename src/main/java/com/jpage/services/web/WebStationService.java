package com.jpage.services.web;

import com.jpage.dto.HealthCheckDto;
import com.jpage.dto.StationListDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.logging.Logger;

/**
 * Hide the access to the microservice inside this local service.
 *
 */
@Service
public class WebStationService {

	@Autowired
	protected RestTemplate restTemplate;

	protected String serviceUrl;

	protected Logger logger = Logger.getLogger(WebStationService.class
			.getName());

	public WebStationService(String serviceUrl) {
		this.serviceUrl = serviceUrl.startsWith("http") ? serviceUrl
				: "http://" + serviceUrl;
	}


	public StationListDto findByName(String stationName) {

		return restTemplate.getForObject(serviceUrl + "/station/{stationName}",
				StationListDto.class, stationName);
	}

	public StationListDto findAll() {

		return restTemplate.getForObject(serviceUrl + "/stations",
				StationListDto.class);
	}


	public HealthCheckDto healthCheck() {
		
		return restTemplate.getForObject(serviceUrl + "/health",
				HealthCheckDto.class);
		
	}
}
