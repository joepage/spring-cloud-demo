package com.jpage.services.web;

public class SearchCriteria {
	private String stationName;

	public String getStationName() {
		return stationName;
	}

	public void setStationName(String stationName) {
		this.stationName = stationName;
	}


	@Override
	public String toString() {
		return "[stationName: " + stationName +"]";
	}
}