package com.jpage.services.station;

import com.jpage.domain.Station;
import com.jpage.dto.StationListDto;
import com.jpage.repository.StationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.logging.Logger;

@RestController
public class StationController {

	protected Logger logger = Logger.getLogger(StationController.class
			.getName());

	protected StationRepository stationRepository;

	@Autowired
	public StationController(StationRepository stationRepository) {
		this.stationRepository = stationRepository;
	}

	/**
	 * Fetch a station with the specified name or part of name in the title.
	 * 
	 */
	@RequestMapping(value = "/station/{stationName}", method = RequestMethod.GET, 
			produces = {MediaType.APPLICATION_JSON_VALUE})
	
	public StationListDto byNameLike(@PathVariable("stationName") String stationName) {

		stationName = stationName.toUpperCase()+"%";
		logger.info("Searching for station with name like " + stationName);
		List<Station> stations = stationRepository.findByNameLike(stationName);

		return new StationListDto(stations);

	}

	/**
	 * Fetch all stations.
	 *
	 */
	@RequestMapping(value = "/stations", method = RequestMethod.GET,
			produces = {MediaType.APPLICATION_JSON_VALUE})

	public StationListDto findAll() {

		logger.info("Searching for all stations");
		List<Station> stations = stationRepository.findAll();

		return new StationListDto(stations);

	}
}
