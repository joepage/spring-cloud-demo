package com.jpage.services.station;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;

import javax.sql.DataSource;
import java.util.logging.Logger;

/**
 * Run as a micro-service, registering with the Discovery Server (Eureka).
 */
@SpringBootApplication
@EntityScan("com.jpage.domain")
@EnableJpaRepositories("com.jpage.repository")
@PropertySource("classpath:db-config.properties")
@EnableAutoConfiguration
@EnableDiscoveryClient
public class StationServer {

	protected Logger logger = Logger.getLogger(StationServer.class.getName());

	/**
	 * Runs the station server service using Spring Boot.
	 * 
	 */
	public static void main(String[] args) {
		System.setProperty("spring.config.name", "station-server");
		SpringApplication.run(StationServer.class, args);
	}

	/**
	 * Creates an in-memory database populated with test data for fast
	 * testing
	 */
	@Bean
	public DataSource dataSource() {
		logger.info("dataSource() invoked");

		// Create an in-memory H2 relational database containing some demo
		// station.
		DataSource dataSource = (new EmbeddedDatabaseBuilder())
				.addScript("classpath:testdb/schema.sql")
				.addScript("classpath:testdb/data.sql").build();

		logger.info("dataSource = " + dataSource);

		return dataSource;
	}
}
