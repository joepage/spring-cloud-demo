package com.jpage.services;

import com.jpage.services.registration.RegistrationServer;
import com.jpage.services.station.StationServer;
import com.jpage.services.web.WebServer;
import org.springframework.boot.SpringApplication;

public class Main {

	public static void main(String[] args) {

		String serverName = "NO-VALUE";

		switch (args.length) {
		case 2:
			// Optionally set the HTTP port to listen on, overrides
			// value in the <server-name>-server.yml file
			System.setProperty("server.port", args[1]);
			// Fall through into ..

		case 1:
			serverName = args[0].toLowerCase();
			break;

		default:
			usage();
			return;
		}

		System.setProperty("spring.config.name", serverName + "-server");

		if (serverName.equals("registration")) {
			SpringApplication.run(RegistrationServer.class, args);
		} else if (serverName.equals("station")) {
			SpringApplication.run(StationServer.class, args);
		} else if (serverName.equals("web")) {
			SpringApplication.run(WebServer.class, args);
		} else {
			System.out.println("Unknown server type: " + serverName);
			usage();
		}
	}

	protected static void usage() {
		System.out.println("Usage: java -jar ... <server-name> [server-port]");
		System.out.println("     where server-name is 'registration', "
				+ "'station' or 'web' and server-port > 1024");
	}
}
