package com.jpage.services.station;

import com.jpage.repository.StationRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.inject.Inject;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = StationServer.class)
@WebIntegrationTest({"server.port:0", "eureka.client.enabled:false"})
public class StationControllerTest {


    @Inject
    private StationRepository stationRepository;

    private MockMvc stationMockMvc;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        StationController stationController = new StationController(stationRepository);
        this.stationMockMvc = MockMvcBuilders.standaloneSetup(stationController).build();
    }

    @Test
    public void testStationNameFull() throws Exception {
        stationMockMvc.perform(get("/station/DERBY"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.stations", hasSize(1)))
                .andExpect(jsonPath("$.stations[0]", hasKey("name")))
                .andExpect(jsonPath("$.stations[0]", hasValue("DERBY")));
    }

    @Test
    public void testStationNamePartial() throws Exception {
        stationMockMvc.perform(get("/station/L"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.stations", hasSize(3)))
                .andExpect(jsonPath("$.stations[0]", hasKey("name")))
                .andExpect(jsonPath("$.stations[0]", hasValue("LIVERPOOL")))
                .andExpect(jsonPath("$.stations[1]", hasKey("name")))
                .andExpect(jsonPath("$.stations[1]", hasValue("LIVERPOOL LIME STREET")))
                .andExpect(jsonPath("$.stations[2]", hasKey("name")))
                .andExpect(jsonPath("$.stations[2]", hasValue("LONDON BRIDGE")));
    }

    @Test
    public void testStationNameNoResults() throws Exception {
        stationMockMvc.perform(get("/station/KINGSX"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.stations", hasSize(0)));
    }

    @Test
    public void testFindAllStations() throws Exception {
        stationMockMvc.perform(get("/stations"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.stations", hasSize(10)))
                .andExpect(jsonPath("$.stations[0]", hasValue("DARTFORD")))
                .andExpect(jsonPath("$.stations[1]", hasValue("DARTMOUTH")))
                .andExpect(jsonPath("$.stations[2]", hasValue("TOWER HILL")))
                .andExpect(jsonPath("$.stations[3]", hasValue("DERBY")))
                .andExpect(jsonPath("$.stations[4]", hasValue("LIVERPOOL")))
                .andExpect(jsonPath("$.stations[5]", hasValue("LIVERPOOL LIME STREET")))
                .andExpect(jsonPath("$.stations[6]", hasValue("PADDINGTON")))
                .andExpect(jsonPath("$.stations[7]", hasValue("EUSTON")))
                .andExpect(jsonPath("$.stations[8]", hasValue("LONDON BRIDGE")))
                .andExpect(jsonPath("$.stations[9]", hasValue("VICTORIA")));
    }
}