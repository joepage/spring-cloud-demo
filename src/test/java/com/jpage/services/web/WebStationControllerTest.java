package com.jpage.services.web;

import com.jpage.domain.Station;
import com.jpage.dto.HealthCheckDto;
import com.jpage.dto.StationListDto;
import com.jpage.exception.ServiceNotFoundException;
import com.jpage.services.station.StationServer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = StationServer.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class WebStationControllerTest {

    @Mock
    WebStationService mockWebStationService;

    MockMvc mockMvc;

    @Before
    public void setUp(){

        MockitoAnnotations.initMocks(this);
        WebStationController webStationController = new WebStationController(mockWebStationService);
        this.mockMvc = MockMvcBuilders.standaloneSetup(webStationController).build();

    }

    @Test
    public void testGetSingleStation() throws Exception {
        StationListDto stationList = getStationTestData(1);
        when(mockWebStationService.findByName(Matchers.anyString())).thenReturn(stationList);

        mockMvc.perform(get("/station/dosearch?stationName=TEST"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.stations", hasSize(1)))
                .andExpect(jsonPath("$.stations[0]", hasKey("name")))
                .andExpect(jsonPath("$.stations[0]", hasValue(stationList.getStations().get(0).getName())));
    }

    @Test
    public void testSearchAll() throws Exception {

        StationListDto stationList = getStationTestData(3);
        when(mockWebStationService.findAll()).thenReturn(stationList);

        mockMvc.perform(get("/station/searchAll"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.stations", hasSize(3)))
                .andExpect(jsonPath("$.stations[0]", hasKey("name")))
                .andExpect(jsonPath("$.stations[0]", hasValue(stationList.getStations().get(0).getName())))
                .andExpect(jsonPath("$.stations[1]", hasKey("name")))
                .andExpect(jsonPath("$.stations[1]", hasValue(stationList.getStations().get(1).getName())))
                .andExpect(jsonPath("$.stations[2]", hasKey("name")))
                .andExpect(jsonPath("$.stations[2]", hasValue(stationList.getStations().get(2).getName())));
    }

    @Test
    public void testStationServiceDown() throws Exception {

        when(mockWebStationService.findAll()).
                thenThrow(new ServiceNotFoundException("TEST SERVICE NOT FOUND"));

        mockMvc.perform(get("/station/searchAll"))
                .andExpect(status().isServiceUnavailable());
    }

    @Test
    public void testNoData() throws Exception {
        StationListDto stationList = getStationTestData(0);
        when(mockWebStationService.findByName(Matchers.anyString())).thenReturn(stationList);
        mockMvc.perform(get("/station/dosearch?stationName=TEST"))
                .andExpect(status().is3xxRedirection());
    }
    
    @Test
    public void testServerStatusHealth() throws Exception {
    	HealthCheckDto healthCheckDto = getHealthCheckDtoData();
        when(mockWebStationService.healthCheck()).thenReturn(healthCheckDto);

        mockMvc.perform(get("/station/health"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.description",containsString(healthCheckDto.getDescription())))
                .andExpect(jsonPath("$.status", is(healthCheckDto.getStatus())));
    }

    private StationListDto getStationTestData(Integer numberOfStations){

        List<Station> stationList = new ArrayList<Station>(numberOfStations);
        String name = "TEST STATION ";

        for (int i=1; i<= numberOfStations; i++){
            Station station =  new Station();
            station.setName(name + i);
            stationList.add(station);
        }

        return new StationListDto(stationList);
    }
    
    
    private HealthCheckDto getHealthCheckDtoData(){
    	HealthCheckDto healthCheckDto = new HealthCheckDto();
    	healthCheckDto.setDescription("TEST SERVICE");
    	healthCheckDto.setStatus("UP");
    	return healthCheckDto;
    }
}