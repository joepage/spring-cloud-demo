package com.jpage.repository;

import com.jpage.domain.Station;
import com.jpage.services.station.StationServer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.inject.Inject;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = StationServer.class)
public class StationRepositoryTest {

    @Inject
    private StationRepository stationRepository;


    @Test
    public void findByAllStations(){
        List<Station> stations = stationRepository.findAll();
        assertTrue(stations.size() == 10);
    }

    @Test
    public void findStationWithNameLike(){
        List<Station> stations = stationRepository.findByNameLike("DART%");
        assertTrue(stations.size() == 2);
    }

    @Test
    public void findStationWithZeroResult(){
        List<Station> stations = stationRepository.findByNameLike("");
        assertTrue(stations.size() == 0);
    }
}